# README #

This is the dedicated server side of my C++-based RTS game project, RTG. The server is written in raw C++, with the client being developed in UE4 (very early skeleton [here](https://bitbucket.org/HateDread/rtg_client/)). The pictures shown below provide a glimpse into the architectural planning I perform when working on this project. I also use Intel VTune to profile, and cppcheck for static analysis. I've spent some time reading and writing x64 assembly, but so far haven't needed it (performance issues are higher-level and cache-bound, so no micro-optimizations). Branches implementing https://github.com/RichieSams/FiberTaskingLib and IntelTBB coming soon.

**NOTE: Make sure to clone/place the [Jaylen](https://bitbucket.org/HateDread/jaylen) physics library into *Ext/Jaylen* inside this repo, and then build it in the desired configuration (i.e. x64 Debug), as rtg_server includes several Jaylen files and links against the Jaylen static library. ** This layout is so I can work on the projects in parallel without inter-project Git interaction (unlike with submodules and subtrees).

* To force rtg_server to start a game without any clients connected, launch it with *-fastStart*.

Some of the algorithms will seem a bit strange, but the gist of it is that objects in the world will receive information about events that occur at different times based on their distance to those events / the speed of light (simulating the time-delay effects of vast distances in space for both visuals and communications). It's early days, but the tech works in the prototypes. I am now building an engine/server around this due to the unique performance requirements of these scenarios, and also so I can learn more about standalone C++.

# Early example diagrams #

*(Open the pictures in a new tab and zoom in - they're of a decently-high resolution).*

Diagramming out parts of the server, inc. how to send and receive packets in a threaded environment:

![Server_Startup.png](https://bitbucket.org/repo/E5eo9K/images/224256070-Server_Startup.png)

Draft of update loop:

![Separate Server - Update.png](https://bitbucket.org/repo/E5eo9K/images/4008734448-Separate%20Server%20-%20Update.png)

Some early musings on cache-coherency in some of the algorithms designed to support the aforementioned space and time delays. They're mostly unchecked, but are at least a demonstration of how I've tried to think about time-critical sections of code that are proving to be a problem. (I back these things up with profiling via VTune etc, but this diagram is slightly outdated and a little incorrect):

![Separate Server - Observer Memory.png](https://bitbucket.org/repo/E5eo9K/images/3926164108-Separate%20Server%20-%20Observer%20Memory.png)