#pragma once

#include "Network/NetClientShared.h"
#include "Network/PacketTypes.h"

namespace Network
{

struct ServerClientDisconnectedMsg
{
	static const EPacketType packetType = EPacketType::SERVER_BROADCAST_DISCONNECTION;
	ClientID disconnectedClient;
};

struct ServerInitialSyncMsg
{
	static const EPacketType packetType = EPacketType::SERVER_INITIAL_SYNC;
	ClientID clientID;
	int numObjects;
};

struct ClientSyncAckMsg
{
	static const EPacketType packetType = EPacketType::CLIENT_SYNC_ACK;
};

} // namespace Network
