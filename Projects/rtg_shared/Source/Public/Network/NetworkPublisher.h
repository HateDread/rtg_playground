#pragma once

#include <map>
#include <vector>
#include <functional>

class INetworkSubscriber;

namespace RakNet
{
struct Packet;
}

namespace Network
{

/**
* Wrapper struct for callbacks fired when RakNet::Packet*s arrive.
* TargetObj is used to identify this callback by which object it is bound to.
*/
struct PacketCallback
{
	template <typename Obj>
	PacketCallback(void(Obj::*mf)(const RakNet::Packet*), Obj* obj)
		: Func(std::bind(mf, obj, std::placeholders::_1)), TargetObj(obj) { }

	// Allows us to accept lambdas, but we require the TargetObj* regardless,
	// otherwise we wouldn't be able to perform a look-up to remove callbacks
	// by target obj (despite lambdas not having/needing one).
	template <typename Obj>
	PacketCallback(const std::function<void(const RakNet::Packet*)>& a_func, Obj* obj)
		: Func(a_func), TargetObj(obj) { }

	std::function<void(const RakNet::Packet*)> Func;
	const void* TargetObj = nullptr;
};

class NetworkPublisher
{
public:
	NetworkPublisher();
	~NetworkPublisher();

	void Register(int a_packetType, PacketCallback a_callback);
	void Register(int a_packetType, std::function<void(const RakNet::Packet*)> a_callback);

	void Update(const std::vector<RakNet::Packet*>& a_packets);

private:
	void NotifySubscribers(int a_packetType, RakNet::Packet* a_packet);

	std::map<int, std::vector<PacketCallback>> m_subscribers;
};

} // namespace Network
