#pragma once

#include <BitStream.h>
#include <array>

namespace Network
{
namespace Serialization
{

template<bool Write, typename T>
void Serialize(RakNet::BitStream& a_stream, T& a_data)
{
	// don't need to skip packet header because it's static
	a_stream.Serialize(Write, (char*)&a_data, sizeof(T));
}

template<typename T>
void Out(RakNet::BitStream& a_outStream, T& a_data)
{
	static_assert(	std::is_same_v<decltype(T::packetType), const EPacketType> ||
					std::is_same_v<decltype(T::packetType), const RakNet::MessageID>, "Message missing const packetType member");

	static_assert(!std::is_member_object_pointer_v<decltype(&T::packetType)>, "packetType must be static");

	a_outStream.Write((EPacketType)a_data.packetType);
	Serialize<true>(a_outStream, a_data);
}

template<typename T>
void In(T& a_outData, RakNet::BitStream& a_outStream)
{
	static_assert(	std::is_same_v<decltype(T::packetType), const EPacketType> ||
					std::is_same_v<decltype(T::packetType), const RakNet::MessageID>, "Message missing const packetType member");

	static_assert(!std::is_member_object_pointer_v<decltype(&T::packetType)>, "packetType must be static");

	a_outStream.IgnoreBytes(sizeof(EPacketType));
	Serialize<false>(a_outStream, a_outData);
}

} // namespace Serialization
} // namespace Network
