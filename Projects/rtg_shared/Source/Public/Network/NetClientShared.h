#pragma once

#include <stdint.h>

namespace Network
{

using ClientID = uint8_t;
const ClientID INVALID_CLIENT = UINT8_MAX;

} // namespace Network
