#pragma once

#include <MessageIdentifiers.h>

namespace Network
{

enum EPacketType : unsigned char
{
	NOT_SET = ID_USER_PACKET_ENUM, // first value must start at ID_USER_PACKET_ENUM
	SERVER_INITIAL_SYNC,
	CLIENT_SYNC_ACK,

	SERVER_BROADCAST_DISCONNECTION,

	SERVER_TEST_MSG
};

} // namespace Network
