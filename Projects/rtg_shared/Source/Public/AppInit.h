#pragma once

#include "CommandArgs.h"
#include <memory>

template <typename T>
class AppInit
{
public:
	AppInit(int argc, const char** argv);

	AppInit(const AppInit&) = delete;
	AppInit& operator=(const AppInit&) = delete;
	
	void Run(const char* a_title);

private:
	std::unique_ptr<T> m_app;
};

template <typename T>
AppInit<T>::AppInit(int argc, const char** argv)
{
	CommandArgs args = CommandArgs(argc, argv);
	m_app = std::make_unique<T>(args);
}

template <typename T>
void AppInit<T>::Run(const char* a_title)
{
	m_app->Run(a_title);
}
