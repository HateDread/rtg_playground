#pragma once

#include <map>

class CommandArgs
{
public:
	CommandArgs(int argc, const char** argv)
	{
		// store no values for now
		for (int i = 0; i < argc; ++i)
		{
			argMap.insert(std::pair<std::string, std::string>(std::string(argv[i]), std::string()));
		}
	}

	/**
	* Useful for arguments that have no associated 'value', i.e. "Debug".
	*/
	bool DoesKeyExist(const std::string& a_key)
	{
		return argMap.find(a_key) != argMap.end();
	}

	void SetVal(const std::string& a_key, const std::string& a_val)
	{
		argMap.insert(std::pair<std::string, std::string>(a_key, a_val));
	}

	bool GetVal(const std::string& a_key, std::string& a_outVal)
	{
		auto it = argMap.find(a_key);
		if (it != argMap.end())
		{
			a_outVal = it->second;
			return true;
		}

		a_outVal = "";
		return false;
	}

	/**
	* Looks for any mention of the given phrase, in any key or value.
	*/
	bool ContainsPhrase(const std::string& a_phrase)
	{
		for (auto it = argMap.begin(); it != argMap.end(); ++it)
		{
			if (it->first.find(a_phrase) != std::string::npos ||
				it->second.find(a_phrase) != std::string::npos)
			{
				return true;
			}
		}
		return false;
	}

private:

	std::map<std::string, std::string> argMap;
};
