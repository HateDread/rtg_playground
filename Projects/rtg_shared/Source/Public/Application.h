#pragma once

#include "CommandArgs.h"

class Application
{
public:
	explicit Application(const CommandArgs& a_commandArgs);
	virtual ~Application();

	void Run(const char* a_title);

	virtual bool Startup() = 0;
	virtual void Shutdown() = 0;
	virtual void Update(double a_deltaTime) = 0;

	void Quit() { m_shouldQuitGame = true; }
	unsigned int GetFPS() const { return m_fps; }

protected:
	// if set to false, the main game loop will exit
	bool			m_shouldQuitGame;
	unsigned int	m_fps;
	double			m_targetFrameTime = (1 / 60.0);
	CommandArgs		m_commandArgs;

};
