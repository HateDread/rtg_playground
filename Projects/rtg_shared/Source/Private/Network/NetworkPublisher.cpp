#include "Network/NetworkPublisher.h"

#include <RakNetTypes.h>

#include <algorithm>

namespace Network
{

NetworkPublisher::NetworkPublisher()
{

}

NetworkPublisher::~NetworkPublisher()
{

}

void NetworkPublisher::Register(int a_packetType, PacketCallback a_callback)
{
	auto it = m_subscribers.find(a_packetType);
	if (it != m_subscribers.end())
	{
		for (int i = 0; i < it->second.size(); ++i)
		{
			// ignore if it's bound to nullptr to allow us to bind multiple lambdas to this packetType
			if (it->second[i].TargetObj == a_callback.TargetObj && a_callback.TargetObj != nullptr)
			{
				printf("Tried to add Subscriber already subscribed to packetType %d\n", a_packetType);
				return;
			}
		}

		it->second.push_back(a_callback);
	}
	else
	{
		auto pair = m_subscribers.emplace(std::pair<int, std::vector<PacketCallback>>(a_packetType, std::vector<PacketCallback>()));
		pair.first->second.push_back(a_callback);
	}
}

void NetworkPublisher::Register(int a_packetType, std::function<void(const RakNet::Packet*)> a_callback)
{
	// ignore the 'TargetObj' pointer normally given, for now - just use nullptr. 
	// (We never access it directly, anyway).
	Register(a_packetType, PacketCallback{ a_callback, (void*)nullptr });
}

void NetworkPublisher::Update(const std::vector<RakNet::Packet*>& a_packets)
{
	for (int i = 0; i < a_packets.size(); ++i)
	{
		const int type = a_packets[i]->data[0];
		NotifySubscribers(type, a_packets[i]);
	}
}

void NetworkPublisher::NotifySubscribers(int a_packetType, RakNet::Packet* a_packet)
{
	auto it = m_subscribers.find(a_packetType);
	if (it != m_subscribers.end())
	{
		for (int i = 0; i < it->second.size(); ++i)
		{
			it->second[i].Func(a_packet);
		}
	}
	else
	{
		printf("NetworkPublisher received packet of type: %d but has no subscribers.\n", a_packetType);
	}
}

} // namespace Network
