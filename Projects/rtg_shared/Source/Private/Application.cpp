#include "Application.h"
#include <time.h>
#include <chrono>

double GetTimeNow()
{
	__int64 millsSinceEpoch =
		std::chrono::system_clock::now().time_since_epoch() /
		std::chrono::milliseconds(1);

	return static_cast<double>(millsSinceEpoch * 0.001);
}

Application::Application(const CommandArgs& a_commandArgs)
	: m_shouldQuitGame(false),
	m_fps(0),
	m_commandArgs(a_commandArgs)
{

}

Application::~Application()
{

}

void Application::Run(const char* title)
{
	// measure from here the first time so 
	// the first frame has a non-zero deltaTime
	double currentTime = GetTimeNow();

	if (Startup())
	{
		unsigned int frames = 0;
		double fpsInterval = 0;

		while (!m_shouldQuitGame)
		{
			// update delta time
			double newTime = GetTimeNow();
			double frameTime = newTime - currentTime;
			currentTime = newTime;

			// stops frameTime running away under heavy load
			if (frameTime > m_targetFrameTime)
			{
				frameTime = m_targetFrameTime;
			}

			// update fps every second
			frames++;
			fpsInterval += frameTime;
			if (fpsInterval > 1.0f) {
				m_fps = frames;
				frames = 0;
				fpsInterval -= 1.0f;
			}

			Update(frameTime);
		}
	}

	Shutdown();
}
