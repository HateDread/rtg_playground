#include "Game/Entity/SpaceID.h"
#include "Network/NetClientShared.h"
#include "Network/PacketTypes.h"
#include "Network/PacketSerialization.h"
#include "Network/Packets/SessionPackets.h"

#include <BitStream.h>
#include <MessageIdentifiers.h>
#include <RakNetTypes.h>
#include <RakPeerInterface.h>

#include <vector>

void GameLoop(RakNet::RakPeerInterface* a_raknet)
{
	RakNet::Packet* packet = a_raknet->Receive();
	while (packet != nullptr)
	{
		switch (packet->data[0])
		{
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				printf("Connection request accepted.\n");

				//RakNet::BitStream output;
				//output.Write((unsigned char)EPacketType::CLIENT_REQUEST_START);

				//a_raknet->Send(&output, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
				break;
			}
			case ID_CONNECTION_LOST:
			{
				printf("Connection lost.\n");
				break;
			}
			case ID_DISCONNECTION_NOTIFICATION:
			{
				printf("Disconnection received.\n");
				break;
			}
			case Network::EPacketType::SERVER_INITIAL_SYNC:
			{
				RakNet::BitStream inStream(packet->data, packet->length, false);

				Network::ServerInitialSyncMsg syncMsg;
				Network::Serialization::In(syncMsg, inStream);

				printf("Receiving initial sync from server:\n");
				printf("Received ID of %d, and number of objects %d\n", syncMsg.clientID, syncMsg.numObjects);

				// reply with sync ack
				RakNet::BitStream outStream;
				Network::Serialization::Out(outStream, Network::ClientSyncAckMsg{});

				a_raknet->Send(&outStream, PacketPriority::HIGH_PRIORITY, PacketReliability::RELIABLE, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

				break;
			}
			default:
			{
				printf("Received packet of type (%d) without a programmed response. \n", packet->data[0]);
			}
		}

		a_raknet->DeallocatePacket(packet);
		packet = a_raknet->Receive();
	}
}

int main()
{
	// might be worth cleaning this all up later

	RakNet::RakPeerInterface* raknet;
	raknet = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor desc;
	raknet->Startup(1, &desc, 1);
	RakNet::ConnectionAttemptResult result = raknet->Connect("127.0.0.1", 12001, nullptr, 0);

	while (true)
	{
		GameLoop(raknet);
		Sleep(DWORD(1000/60.0));
	}

	raknet->Shutdown(100);
	RakNet::RakPeerInterface::DestroyInstance(raknet);

	return 0;
}