
#include "AppInit.h"
#include "ServerApplication.h"

int main(int argc, const char** argv)
{
	AppInit<ServerApplication> app(argc, argv);
	app.Run("RTG_Server");

	return 0;
}
