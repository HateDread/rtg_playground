#pragma once

#include <functional>
#include <algorithm>
#include <vector>

using CallbackHandle = unsigned int;
template <int CType, typename... Args> class StaticEvent;

// Template class for storing callback info
template<int CType, typename... Args>
class CallbackStorage {
private:
	friend class StaticEvent<CType, Args...>;

	template<typename O>
	static CallbackHandle SetupLambda(void(O::*objFunc)(Args...), O* obj) {
		BoundLambda bound{ [objFunc, obj](Args&&... args) {
			(obj->*objFunc)(std::forward<Args>(args)...);
		}, nextHandle++ };

		lambdas.push_back(bound);
		return bound.handle;
	}

	// Take std::function directly for lambda support
	static CallbackHandle SetupLambda(std::function<void(Args...)>&& objFunc/*, O* obj*/) {
		BoundLambda bound{ objFunc, nextHandle++ };
		lambdas.push_back(bound);
		return bound.handle;
	}

	static void CallLambda(Args&&... args) {
		for (unsigned int i = 0; i < lambdas.size(); ++i)
		{
			// Can't forward the args since we're using them multiple times
			lambdas[i].lambda(args...);
		}
	}

private:
	struct BoundLambda
	{
		std::function<void(Args...)> lambda;
		CallbackHandle handle;
	};

	static std::vector<BoundLambda> lambdas;
	static CallbackHandle nextHandle;
};

// Init statics
template <int CType, typename... Args>
CallbackHandle CallbackStorage<CType, Args...>::nextHandle = 0;
template <int CType, typename... Args>
std::vector<typename CallbackStorage<CType, Args...>::BoundLambda> CallbackStorage<CType, Args...>::lambdas;

template <int CType, typename... Args>
class StaticEvent
{
public:
	// Member function support
	template <typename O>
	static CallbackHandle Subscribe(void(O::*objFunc)(Args...), O* obj) {
		return CallbackStorage<CType, Args...>::SetupLambda(objFunc, obj);
	}

	// Lambda (without template deduction) and functor support
	static CallbackHandle Subscribe(std::function<void(Args...)>&& objFunc/*, O* obj*/) {
		return CallbackStorage<CType, Args...>::SetupLambda(std::forward<std::function<void(Args...)>>(objFunc)/*, obj*/);
	}

	static void Unsubscribe(CallbackHandle handle) {
		auto& lambdas = CallbackStorage<CType, Args...>::lambdas;
		auto it = std::find_if(lambdas.begin(), lambdas.end(), [handle](auto&& lambda) {
			return handle == lambda.handle;
		});
		if (it != lambdas.end())
		{
			lambdas.erase(it);
		}
	}

	static void Clear() {
		CallbackStorage<CType, Args...>::lambdas.clear();
	}

	static void Invoke(Args... args) {
		CallbackStorage<CType, Args...>::CallLambda(std::forward<Args>(args)...);
	}
};
