#pragma once

#include "Network/NetClientShared.h"

namespace RakNet
{
struct RakNetGUID;
}

namespace Network
{

ClientID RegisterClient(const RakNet::RakNetGUID& a_netGUID);

void RemoveClient(ClientID a_clientID);
void RemoveClient(const RakNet::RakNetGUID& a_netGUID);

const RakNet::RakNetGUID& GetConnectionGUID(ClientID a_clientID);
ClientID GetClientID(const RakNet::RakNetGUID& a_netGUID);

int NumClients();

} // namespace Network
