#pragma once

#include "Network/NetClient.h"

#include <vector>
#include <functional>
#include <unordered_map>
#include <assert.h>

namespace Network
{

struct ServerInitialSyncMsg;

class SyncManager
{
public:
	using SyncCallback = std::function<void(bool)>;

public:
	void BeginSync(const ServerInitialSyncMsg& a_syncMsg, const SyncCallback& a_syncCallback = SyncCallback());
	void OnSyncAck(ClientID a_clientID);

	int NumPlayersLoading();
	int NumPlayersLoaded();

	void ResetNumLoaded();

private:
	std::unordered_map<ClientID, SyncCallback> m_loadingPlayers;
	int m_numLoaded = 0;
};

} // namespace Network
