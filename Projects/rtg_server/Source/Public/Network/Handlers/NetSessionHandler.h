#pragma once

class GameFramework;

namespace RakNet
{
struct Packet;
}

namespace Network
{

class NetworkPublisher;

class NetSessionHandler
{
public:
	NetSessionHandler(GameFramework* a_gameFramework, NetworkPublisher* a_networkPublisher);

private:
	void OnNewConnection(const RakNet::Packet* a_packet);
	void OnDisconnection(const RakNet::Packet* a_packet);
	void OnClientSyncAck(const RakNet::Packet* a_packet);

	GameFramework* m_gameFramework;
};

} // namespace Network
