#pragma once

#include <memory>

class GameFramework;
class NetInputHandler;

namespace Network
{

class NetworkPublisher;
class NetSessionHandler;

class NetHandlers
{
public:
	NetHandlers(GameFramework* a_gameFramework, NetworkPublisher* a_networkPublisher);
	~NetHandlers();

private:
	//std::unique_ptr<NetInputHandler> m_inputHandler;
	std::unique_ptr<NetSessionHandler> m_sessionHandler;

};

} // namespace Network
