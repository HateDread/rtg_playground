#pragma once

#include "Network/NetClientShared.h"
#include "Network/PacketSerialization.h"

#include <PacketPriority.h>
#include <BitStream.h>

#include <vector>
#include <memory>

namespace RakNet
{
class RakPeerInterface;
class BitStream;
}

namespace Network
{

class INetworkMessage;
enum EPacketType : unsigned char;

namespace Outgoing
{

// Selective stream
void EnqueueStream(const RakNet::BitStream& a_message, const std::vector<ClientID>& a_targetIDs,
	PacketPriority a_priority, PacketReliability a_reliability, int a_orderingChannel = 0);

// Broadcast stream to all
void BroadcastStream(const RakNet::BitStream& a_message, PacketPriority a_priority, 
	PacketReliability a_reliability, int a_orderingChannel = 0);

void SendAll(RakNet::RakPeerInterface* a_raknet);

// Selective message
template<typename T>
void EnqueueMessage(T&& a_message, const std::vector<ClientID>& a_targetIDs,
	PacketPriority a_priority, PacketReliability a_reliability, int a_orderingChannel = 0)
{
	static_assert(!(std::is_same<T, RakNet::BitStream>::value), "Trying to enqueue BitStream as a message");

	RakNet::BitStream outStream;
	Serialization::Out(outStream, a_message);

	EnqueueStream(outStream, a_targetIDs, a_priority, a_reliability, a_orderingChannel);
}

// Broadcast message to all
template<typename T>
void BroadcastMessage(T&& a_message, PacketPriority a_priority,
	PacketReliability a_reliability, int a_orderingChannel = 0)
{
	static_assert(!(std::is_same<T, RakNet::BitStream>::value), "Trying to enqueue BitStream as a message");

	RakNet::BitStream outStream;
	Serialization::Out(outStream, a_message);

	BroadcastStream(outStream, a_priority, a_reliability, a_orderingChannel);
}

} // namespace Outgoing
} // namespace Network
