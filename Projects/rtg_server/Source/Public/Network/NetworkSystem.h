#pragma once

#include <memory>
#include <vector>

namespace RakNet
{
class RakPeerInterface;
}

namespace Network
{

class NetworkPublisher;

class NetworkSystem
{
public:

	NetworkSystem();
	~NetworkSystem();

	void Update();
	void PostUpdate();

	NetworkPublisher* GetPublisher();

private:

	std::unique_ptr<NetworkPublisher> m_publisher;

	// PeerInterface in here for now - much simpler
	RakNet::RakPeerInterface* m_raknet;
};

} // namespace Network
