#pragma once

#include "Application.h"
#include <memory>

class GameFramework;

namespace Network
{
class NetworkSystem;
class NetHandlers;
}

class ServerApplication : public Application
{
public:
	explicit ServerApplication(const CommandArgs& a_commandArgs);
	virtual ~ServerApplication();

	virtual bool Startup() override;
	virtual void Shutdown() override;
	virtual void Update(double a_deltaTime) override;

private:
	void DoUpdate(double a_deltaTime);

	std::unique_ptr<GameFramework>			m_game;
	std::unique_ptr<Network::NetworkSystem>	m_network;
	std::unique_ptr<Network::NetHandlers>	m_handlers;

	struct
	{
		double step = 1 / 60.0;
		double accumulator = 0.0;
	} m_frameTimer;
};
