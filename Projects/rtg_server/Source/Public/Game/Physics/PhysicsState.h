#pragma once

#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

// 64 byte physics state
struct PhysicsState
{
	PhysicsState() = default;
	PhysicsState(const glm::dvec3& a_pos, const glm::vec3& a_vel, const glm::quat& a_orient, float a_time)
		: pos(a_pos), vel(a_vel), orient(a_orient), time(a_time) {}

	glm::dvec3 	pos; 	// 24 bytes
	glm::vec3 	vel; 	// 12 bytes
	glm::quat	orient;	// 16 bytes
	float		time; 	// 04 bytes
};						// = pads to 64 bytes
