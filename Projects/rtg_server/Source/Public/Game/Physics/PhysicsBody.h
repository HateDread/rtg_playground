#pragma once

#include "glm/vec3.hpp"
#include "glm/gtc/quaternion.hpp"

class JayObject;

class PhysicsBody
{
	friend class PhysicsManager;

public:
	explicit PhysicsBody(JayObject* a_jayObject);
	PhysicsBody() : m_jayObject(nullptr) {}

	glm::dvec3 GetPosition() const;
	glm::vec3 GetVelocity() const;
	glm::vec3 GetForce() const;
	float GetMass() const;

	glm::quat GetOrientation() const;
	glm::vec3 GetAngularVelocity() const;
	glm::vec3 GetTorque() const;
	float GetMomentOfInertia() const;

	//glm::vec3 GetForwardVector() const;

	// Setters

	void AddForce(const glm::vec3& a_force);
	void AddTorque(const glm::vec3& a_torque);

	void AddRelativeForce(const glm::vec3& a_force);
	void AddRelativeTorque(const glm::vec3& a_torque);

	void SetVelocity(const glm::vec3& a_velocity);
	void SetRelativeVelocity(const glm::vec3& a_velocity);

	void SetPosition(const glm::dvec3& a_position);

	void SetOrientation(const glm::quat& a_orientation);
	void SetOrientation(const glm::vec3& a_euler);
	void SetOrientationRad(const glm::vec3& a_rads);
	void SetOrientation(float a_angle, const glm::vec3& a_axis);

	void SetMass(float a_mass);
	void SetMomentOfInertia(float a_momentOfInertia);

public:

	bool operator==(const PhysicsBody& rhs) const
	{
		return m_jayObject == rhs.m_jayObject;
	}

private:
	JayObject* m_jayObject = nullptr;

};

