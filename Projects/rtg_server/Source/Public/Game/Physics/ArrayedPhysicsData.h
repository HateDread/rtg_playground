#pragma once

#include <vector>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

struct ArrayedPhysicsData
{
	std::vector<glm::dvec3> positions;
	std::vector<glm::vec3> velocities;
	std::vector<glm::quat> orientations;
};