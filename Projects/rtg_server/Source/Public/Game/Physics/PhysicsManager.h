#pragma once

#include <memory>
#include <vector>
#include "Game/Physics/PhysicsBody.h"


class Jaylen;
class JayScene;
struct ArrayedPhysicsData;

class PhysicsManager
{
public:
	PhysicsManager();
	~PhysicsManager();

	PhysicsManager(const PhysicsManager&) = delete;
	PhysicsManager& operator=(const PhysicsManager&) = delete;

	void CreateScene(unsigned int a_maxObjects);
	void Update(double a_deltaTime);

	void FillArrayedData(ArrayedPhysicsData& a_arrayedPhysData);

	PhysicsBody CreatePhysicsBody();
	void DestroyPhysicsBody(const PhysicsBody& a_physicsBody);

	inline const std::vector<PhysicsBody>& GetPhysicsBodies() const { return m_physicsBodies; }

private:
	std::unique_ptr<Jaylen> m_jaylen;
	std::vector<PhysicsBody> m_physicsBodies;
};
