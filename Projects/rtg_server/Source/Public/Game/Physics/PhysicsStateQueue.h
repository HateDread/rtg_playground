#pragma once

#include <unordered_map>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <vector>
#include "Game/Entity/ECS.h"
#include "ArrayedPhysicsData.h"
#include "Game/Physics/PhysicsState.h"

class PhysicsStateQueue
{
public:


public:
	/**
	* Uses the given physics data to create a new vector of states
	* @return The new index/id of the PhysicsStates created.
	*/
	void CreateNewStates(const ArrayedPhysicsData& a_physData, double a_timestamp);
	void ReserveSpaceForObjects(int a_numObjects);

	auto& operator[](unsigned int idx) const {
		return m_objectPhysStates[idx];
	}

private:

	std::vector<std::vector<PhysicsState>> m_objectPhysStates;

};
