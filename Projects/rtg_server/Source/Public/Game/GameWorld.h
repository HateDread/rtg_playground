#pragma once

#include "Game/Entity/ECS.h"
#include "Game/Entity/SpaceObjects.h"

class GameWorld
{
public:
	GameWorld() = default;
	GameWorld(const GameWorld& rhs)
		: m_entities(rhs.m_entities) {}

	ComponentManager& GetEntities() { return m_entities; }
	const ComponentManager& GetEntities() const { return m_entities; }

	SpaceObjects& GetSpaceObjects() { return m_spaceObjects; }
	const SpaceObjects& GetSpaceObjects() const { return m_spaceObjects; }

private:
	ComponentManager m_entities;
	SpaceObjects m_spaceObjects;

};
