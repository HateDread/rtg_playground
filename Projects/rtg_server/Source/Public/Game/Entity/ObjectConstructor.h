#pragma once

#include "Game/Entity/ECS.h"
#include "Game/Entity/SpaceObject.h"

class PhysicsManager;
class PhysicsBody;
class ObserverManager;
class GameWorld;
class SpaceObjects;

class ObjectConstructor
{
public:
	explicit ObjectConstructor(PhysicsManager* a_physicsManager);

	SpaceObject CreateSpaceObject(ComponentManager& a_components, ObserverManager& a_observerManager, SpaceObjects& a_spaceObjects) const;
	SpaceObject CreateShip(GameWorld& a_realWorld, ObserverManager& a_observerManager, SpaceObjects& a_spaceObjects) const;

private:
	inline EntityBase CreateEntity(ComponentManager& a_components) const;

	PhysicsManager* m_physicsManager;
	static SpaceID m_nextID;

};
