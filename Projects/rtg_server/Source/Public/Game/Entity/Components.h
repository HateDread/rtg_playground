#pragma once

#include "glm/vec3.hpp"
#include "Game/Physics/PhysicsBody.h"

struct PosCom
{
	glm::dvec3 p;
};

struct VelCom
{
	glm::dvec3 v;
};

struct OrientCom
{
	glm::quat o;
};

struct PhysCom
{
	PhysicsBody body;
};
