/************************************************************************/
/* ECS library provided by Masstronaut/AllanDeutsch						*/
/* https://github.com/Masstronaut										*/
/************************************************************************/

#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <utility>
#include <tuple>
#include <typeinfo>
#include <unordered_map>
#include <cassert>
#include <algorithm>
#include <functional>
#include <queue>
#include <array>


using EntityID = unsigned int;

#pragma region tuple_unpack_impl
// This code is used to unpack tuple elements into function arguments. 
// Any additional arguments you want to pass can be added after the tuple, and will be unpacked first, IE:
// apply(tuple<int,bool,char>, float, double) would be unpacked as f(float,double,int,bool,char)
// This code is used to power the functions which work as systems in the ECS model, referred to as updaters below.
template<size_t N>
struct Apply {
	template<typename F, typename T, typename... A>
	static inline auto apply(F && f, T && t, A &&... a) {
		return Apply<N - 1>::apply(::std::forward<F>(f), ::std::forward<T>(t),
			::std::forward<A>(a)...,
			::std::get<std::tuple_size<std::decay_t<T>>::value - N >(::std::forward<T>(t))
		);
	}
};
template<>
struct Apply<0> {
	template<typename F, typename T, typename... A>
	static inline auto apply(F && f, T &&, A &&... a) {
		return f(::std::forward<A>(a)...);
	}
};

template<typename F, typename T, typename... Args>
inline auto apply(F && f, T && t, Args&&... args) {
	return Apply< ::std::tuple_size< ::std::decay_t<T>
	>::value>::apply(::std::forward<F>(f), ::std::forward<T>(t), ::std::forward<Args>(args)...);
}
#pragma endregion

#pragma region Components
static constexpr EntityID INVALID_ID{ UINT32_MAX };
// Credit to Sean Parent for this snippet
template <typename Pred, typename... Args>
void for_each_argument(Pred p, Args&&... args) {
	using arrT = int[];
	static_cast<void>(arrT{ (f(std::forward<Args>(args)), 0)... });
}
class ComponentStoreBase {
public:
	virtual EntityID front() const = 0;
	virtual EntityID back() const = 0;
	// @@TODO: make these O(1)
	virtual EntityID next(EntityID) const = 0;
	virtual EntityID find(EntityID) const = 0; // returns the ID if found, or the first greater ID.
	virtual EntityID operator[](unsigned) const = 0;
	virtual size_t size() const = 0;
	virtual void remove(EntityID) = 0;
	virtual size_t index_of(EntityID entity) const = 0;
	virtual std::pair<size_t, EntityID> advance_index(size_t index, EntityID entity) const = 0;
	virtual ~ComponentStoreBase() {}

	// #Brody - deep-copy func required in interface so we can clone off from a base class ptr
	virtual std::unique_ptr<ComponentStoreBase> clone() = 0;
};

template<typename Component>
class ComponentStore : public ComponentStoreBase {
public:
	ComponentStore() = default;

	// #Brody
	ComponentStore(const ComponentStore& rhs)
		: m_components(rhs.m_components),
		m_IDs(rhs.m_IDs) { }

	ComponentStore<Component>& operator=(const ComponentStore&) = delete;

	// #Brody
	std::unique_ptr<ComponentStoreBase> clone() override
	{
		return std::make_unique<ComponentStore<Component>>(*this);
	}

	Component* get(EntityID entity) {
		auto index{ lower_bound(entity) };
		if (index < m_components.size()) {
			return &m_components[index];
		}
		else {
			return nullptr;
		}
	}
	Component* get(EntityID entity, size_t index) {
		assert(index < m_IDs.size());
		assert(m_IDs[index] == entity);
		return &m_components[index];
	}

	// #Brody - added for the ability to iterate all
	// components of this ComponentStore's type
	std::vector<Component>& get()
	{
		return m_components;
	}

	const std::vector<Component>& get() const
	{
		return m_components;
	}

	void* compData() const {
		return (void*)m_components.data();
	}

	virtual size_t size() const final {
		return m_IDs.size();
	}
	virtual EntityID front() const final {
		return m_IDs.front();
	}
	virtual EntityID back() const final {
		return m_IDs.back();
	}
	virtual EntityID next(EntityID curr) const final {
		auto it = std::lower_bound(m_IDs.begin(), m_IDs.end(), curr);
		if (it == m_IDs.end()) {
			return INVALID_ID;
		}
		else if (*it > curr) {
			return *it;
		}
		else if (*it == curr && ++it != m_IDs.end()) {
			return *it;
		}
		else {
			return INVALID_ID;
		}

	}
	virtual EntityID find(EntityID val) const final {
		auto it = std::lower_bound(m_IDs.begin(), m_IDs.end(), val);
		if (it != m_IDs.end()) {
			return *it;
		}
		else {
			return INVALID_ID;
		}
	}
	virtual size_t index_of(EntityID entity) const final {
		auto it{ std::lower_bound(m_IDs.begin(), m_IDs.end(), entity) };
		if (*it == entity) {
			return std::distance(m_IDs.begin(), it);
		}
		else {
			return INVALID_ID;
		}
	}
	virtual void remove(EntityID entity) final {
		size_t index{ index_of(entity) };
		auto idit{ m_IDs.begin() };
		auto cmpit{ m_components.begin() };
		std::advance(idit, index);
		std::advance(cmpit, index);
		m_IDs.erase(idit);
		m_components.erase(cmpit);
	}
	virtual std::pair<size_t, EntityID> advance_index(size_t index, EntityID entity) const final {
		while (index < m_IDs.size() && m_IDs[index] < entity) {
			++index;
		}
		if (index >= m_IDs.size()) {
			return{ INVALID_ID, INVALID_ID };
		}
		return{ index, m_IDs[index] };
	}

	virtual EntityID operator[](unsigned index) const final {
		if (index < m_IDs.size()) {
			return m_IDs[index];
		}
		return INVALID_ID;
	}

	Component* at(unsigned index) const {
		if (index < m_data.size()) {
			return &(m_data[index].second);
		}
		return nullptr;
	}
	void get(EntityID entity, Component*& prcomp) {
		// TODO: change to binary search
		// find the value if it exists
		for (std::pair<EntityID, Component> &component : m_data) {
			if (entity < component.first) {
				continue;
			}
			else if (entity > component.first) {
				break; // passed the ID in a sorted container
			}
			else { // ID matches
				prcomp = component.second;
				return;
			}
		}
		prcomp = nullptr;
	}
	size_t lower_bound(EntityID entity) const {
		size_t count{ m_IDs.size() }, first{ 0 }, last{ m_IDs.size() };

		while (0 < count) {
			size_t half_count{ count / 2 };
			size_t mid{ first };
			mid += half_count;
			if (m_IDs[mid] < entity) {
				first = ++mid;
				count -= half_count + 1;
			}
			else {
				count = half_count;
			}
		}

		return first;
	}
	template<typename... Args>
	Component* add(EntityID entity, Args&&... args) {
		assert(entity != INVALID_ID && entity != 0);
		if (!m_components.size() || m_IDs.back() < entity) {
			m_components.emplace_back(std::move(Component{ std::forward<Args>(args)... }));
			m_IDs.emplace_back(entity);
			return &m_components[0];
		}
		size_t index{ lower_bound(entity) };
		if (index < m_IDs.size()) {
			assert(m_IDs[index] != entity && "Attempted to add a component to an entity which already has it.");
			auto IDit{ m_IDs.begin() + index };
			auto compit{ m_components.begin() + index };
			m_IDs.insert(IDit, entity);
			m_components.insert(compit, std::move(Component{ std::forward<Args>(args)... }));
			return &m_components[index];
		}
		else {
			return nullptr;
		}
	}
	void kill(EntityID entity) {}
private:
	std::vector<Component> m_components;
	std::vector<EntityID> m_IDs;

};

class ComponentManager {
public:
	using ComponentStorePtr = std::unique_ptr<ComponentStoreBase>;

	ComponentManager() = default;
	// #Brody - allows us to deep-copy construct a ComponentManager
	ComponentManager(const ComponentManager& rhs)
		: m_nextID(rhs.m_nextID),
		m_updaters(rhs.m_updaters) 
	{ 
		// deep copy the actual component vectors
		m_store.reserve(rhs.m_store.size());
		for (auto it = rhs.m_store.begin(); it != rhs.m_store.end(); ++it)
		{
			m_store.insert({ it->first,  it->second->clone()});
		}
	}

	ComponentManager& operator=(const ComponentManager&) = delete;

	// #Brody - convenience to let ComponentManager keep track of next ID when creating new Entities
	EntityID GetNextID() { return m_nextID++; }

	template<typename Component>
	void register_type() {
		register_type(typeid(Component).hash_code(), ComponentManager::ComponentStorePtr(new ComponentStore<Component>()));
	}

	template<typename Component>
	ComponentStore<Component>* get() {
		auto iter{ m_store.find(typeid(Component).hash_code()) };
		if (iter != m_store.end()) {
			return reinterpret_cast<ComponentStore<Component>*>(iter->second.get());
		}
		else {
			register_type<Component>();
			return get<Component>();
		}
	}

	template<typename T>
	T* get(EntityID id) {
		return get<T>()->get(id);
	}

	// #Brody
	template<typename Component>
	const ComponentStore<Component>* get() const {
		auto iter{ m_store.find(typeid(Component).hash_code()) };
		if (iter != m_store.end()) {
			return reinterpret_cast<const ComponentStore<Component>*>(iter->second.get());
		}
		return nullptr;
	}

#pragma region iterators
	template<typename... Args>
	class iterator {
	public:
		iterator(ComponentManager& manager)
			: m_manager(manager)
			, m_curr_id(INVALID_ID) {
			for (unsigned i{ 0 }; i < m_IDs.size(); ++i) {
				m_indices[i] = 0;
				m_IDs[i] = INVALID_ID;
			}
			update_IDs<Args...>();
			auto it = id_ceils_matched();
			if (it.first && it.second != INVALID_ID) {
				m_curr_id = it.second;
			}
		}

		bool valid(EntityID entity) const {
			if (entity == INVALID_ID) return false;
			bool is_valid{ true };
			valid_helper<Args...>(is_valid, entity);
			return is_valid;
		}
		operator bool() const {
			return valid(m_curr_id);
		}
		iterator<Args...>& operator++() {
			next();
			return *this;
		}
		iterator<Args...> operator++(int) {
			auto ret = *this;
			next();
			return ret;
		}

		EntityID operator*() const {
			return m_curr_id;
		}

		template<typename T, typename... GetArgs>
		void get(T*& component, GetArgs*&... args) const {
			get_helper<T, Args...>(component, 0);
			get<GetArgs...>(args...);
		}
		template<typename T>
		void get(T*& component) const {
			get_helper<T, Args...>(component, 0);
		}
	private:
		template<typename T, typename T2, typename T3, typename... GetArgs>
		void get_helper(T*& component, unsigned index) const {
			if (std::is_same<T, T2>::value) {
				component = m_manager.get<T>()->get(m_curr_id);
			}
			else {
				get_helper<T, T3, GetArgs...>(component, ++index);
			}
		}
		template<typename T, typename U>
		void get_helper(T*& component, unsigned index) const {
			if (std::is_same<T, U>::value) {
				component = m_manager.get<T>()->get(m_curr_id);
			}
			else {
				component = nullptr;
			}
		}

		template<typename T, typename T2, typename... Args>
		void update_IDs(unsigned index = 0) {
			update_IDs<T>(index);
			update_IDs<T2, Args...>(++index);
		}

		template<typename T>
		void update_IDs(unsigned index = 0) {
			if (m_indices[index] < m_manager.get<T>()->size()) {
				m_IDs[index] = (*m_manager.get<T>())[m_indices[index]];
			}
			else {
				m_IDs[index] = INVALID_ID;
			}
		}

		bool next() {
			std::pair<bool, EntityID> matching = id_ceils_matched();
			// ONCE: advance all the IDs to the next valid ID
			if (matching.second == INVALID_ID) {
				for (auto &&it : m_IDs) {
					it = 0;
				}
				for (auto &&it : m_indices) {
					it = 0;
				}
				update_IDs<Args...>();
				matching = id_ceils_matched();
				if (matching.first && !valid(matching.second)) {
					advance_ID<Args...>(0);
					matching = id_ceils_matched();
				}
			}
			else {
				advance_ID<Args...>(0);
				matching = id_ceils_matched();
			}

			if (matching.first && valid(matching.second)) {
				m_curr_id = matching.second;
				return true;
			}

			// while all the IDs aren't matching
			while (!matching.first || !valid(matching.second)) {
				// Find the highest ID among all the gathered IDs
				// Advance all other IDs
				advance_ID<Args...>(0, matching.second);
				matching = id_ceils_matched();
				if (matching.second == INVALID_ID) {
					m_curr_id = INVALID_ID;
					return false;
				}
			}
			m_curr_id = matching.second;
			return true;

		}

		template<typename Component>
		void valid_helper(bool& result, EntityID entity) const {
			if (entity != m_manager.get<Component>()->find(entity)) {
				result = false;
			}
		}
		template<typename Component, typename Component2, typename... Args>
		void valid_helper(bool& result, EntityID entity) const {
			valid_helper<Component>(result, entity);
			valid_helper<Component2, Args...>(result, entity);
		}

		/*
		Advance all IDs
		*/
		template<typename Component>
		void advance_ID(unsigned index) {
			auto* mgr = m_manager.get<Component>();
			if (m_indices[index] + 1 < mgr->size()) {
				m_indices[index] += 1;
				m_IDs[index] = (*mgr)[m_indices[index]];
			}
			else {
				m_indices[index] = INVALID_ID;
				m_IDs[index] = INVALID_ID;
				m_curr_id = INVALID_ID;
			}
		}
		template<typename Component, typename Component2, typename... Args>
		void advance_ID(unsigned index) {
			advance_ID<Component>(index);
			advance_ID<Component2, Args...>(++index);
		}
		/*
		Advance to lower_bound(value)
		*/
		//@@TODO: modify lower bound versions to also update the indices array
		template<typename Component>
		void advance_ID(unsigned index, EntityID value) {
			auto data{ m_manager.get<Component>()->advance_index(m_indices[index], value) };
			m_IDs[index] = data.second;
			m_indices[index] = data.second;
		}
		template<typename Component, typename Component2, typename... Args>
		void advance_ID(unsigned index, EntityID value) {
			advance_ID<Component>(index, value);
			advance_ID<Component2, Args...>(++index, value);
		}

		std::pair<bool, EntityID> id_ceils_matched() const {
			bool matched = true;
			EntityID ceil = m_IDs[0];
			for (auto &&it : m_IDs) {
				if (it == INVALID_ID) {
					return{ false, INVALID_ID };
				}
				if (it > ceil) {
					matched = false;
					ceil = it;
				}
				else if (it == ceil) {
					continue;
				}
				else {
					matched = false;
				}
			}
			return{ matched, ceil };
		}

	private:
		std::array<EntityID, sizeof...(Args)> m_IDs{ INVALID_ID };
		std::array<unsigned, sizeof...(Args)> m_indices{ INVALID_ID };
		ComponentManager& m_manager;
		EntityID m_curr_id{ INVALID_ID };
	};
#pragma endregion

#pragma region interface
	void update(float dt) {
		for (auto& it : m_updaters) it(dt);
	}

	template<typename ReturnType, typename... Args>
	void register_updater(ReturnType(*fn)(float, Args*...), const std::string&& name) {
		m_updaters.emplace_back(updater{ std::move(name),
			[this, fn](float dt) {
			auto iter{ this->begin<Args...>() };
			while (iter) {
				EntityID entity{ *iter };
				auto component_tuple{ this->GetComponents<Args...>(entity) };
				// unpack the tuple into a list of arguments to the function, of the form fn(dt, args...)
				apply(fn, component_tuple, dt);
				++iter;
			}
		}
		}
		);

	}
	template<typename... Args>
	void register_updater(std::function<void(Args...)>&& fn, const std::string&& name) {
		static_assert("working as intended");
	}
	template<typename... Args>
	iterator<Args...> begin() {
		auto it{ iterator<Args...>(*this) };
		if (it) {
			return it;
		}
		else {
			return ++it;
		}
	}
	template<typename... Args>
	iterator<Args...> end() {
		return iterator<Args...>(*this);
	}

	// for each tuple element, pass a reference to the data pointer to a variadic function which fills them out
	template<typename... Args>
	std::tuple<Args*...> GetComponents(EntityID entity) {
		return GetComponentsHelper<std::remove_pointer_t<Args>...>(entity);
	}
	template<typename T>
	void GetComponents(EntityID entity, T*& component) {
		GetComponent(entity, component);
	}
	template<typename T, typename... Args>
	void GetComponents(EntityID entity, T*& arg1, Args*&... args) {
		GetComponent(entity, arg1);
		GetComponents(entity, args...);
	}

	void kill(EntityID entity) {
		for (auto& it : m_store) {
			it.second->remove(entity);
		}
	}

#pragma endregion

private:
#pragma region Updaters
	class updater {
	public:
		updater(const std::string&& Name, std::function<void(float)>&& Fn)
			: name(std::move(Name))
			, fn(std::move(Fn))
		{}
		inline void operator()(float dt) const { fn(dt); }
		std::string name;
		std::function<void(float)> fn;
	};
#pragma endregion
	void register_type(size_t hash, ComponentStorePtr &&manager) {
		assert(m_store.find(hash) == m_store.end());
		m_store.emplace(hash, std::move(manager));
	}

#pragma region GetComponentHelpers
	template<typename T>
	T* GetComponent(EntityID entity) {
		return get<T>()->get(entity);
	}

	template<typename... Args>
	std::tuple<Args*...> GetComponentsHelper(EntityID entity) {
		return std::make_tuple(GetComponent<Args>(entity)...);
	}

	template<typename T>
	void GetComponent(EntityID entity, T*& component) {
		component = get<T>()->get(entity);
	}

#pragma endregion

	// #Brody
	EntityID m_nextID = 1;
	std::unordered_map<size_t, ComponentStorePtr> m_store;
	std::vector< updater > m_updaters;
};
#pragma endregion

// Example entity wrapper:
#pragma region Entity
class EntityBase {
public:
	EntityBase(EntityID self, ComponentManager* data)
		: m_self(self)
		, m_data(data) {
	}

	// #Brody - added to let ComponentManager assign next ID
	EntityBase(ComponentManager* data)
		: m_data(data)
	{
		m_self = EntityID{data->GetNextID()};
	}

	virtual ~EntityBase() {}
	EntityBase(const EntityBase& rhs)
		: m_self(rhs.m_self),
		m_data(rhs.m_data)
	{
		
		
	}

	EntityBase& operator=(const EntityBase&) = delete;

	operator EntityID() { return m_self; }

	template<typename... Args>
	void get(Args*&... args) {
		if (m_data) {
			m_data->GetComponents(m_self, std::forward<Args*&>(args)...);
		}
	}

	template<typename T, typename... Args>
	T* add(Args&&... args) {
		if (m_data) {
			return m_data->get<T>()->add(m_self, std::forward<Args>(args)...);
		}
		else {
			return nullptr;
		}
	}

private:
	EntityID m_self{ 0 };
	ComponentManager* m_data{ nullptr };
};
#pragma endregion