#pragma once
#include <vector>
#include <unordered_map>
#include "SpaceObject.h"

class SpaceObjects
{
public:

	void AddObject(SpaceObject& a_object) {
		m_spaceObjects.push_back(a_object);
		m_IDToIndex.insert(std::make_pair(a_object.m_ID, m_spaceObjects.size() - 1));
		m_IDs.push_back(a_object.m_ID);
	}

	SpaceObject& GetObject(SpaceID a_ID) {
		return m_spaceObjects[m_IDToIndex[a_ID]];
	}

	std::vector<SpaceObject>& Data() {
		return m_spaceObjects;
	}

	const std::vector<SpaceObject>& Data() const {
		return m_spaceObjects;
	}

	const std::vector<SpaceID>& GetIDs() const {
		return m_IDs;
	}

private:
	std::vector<SpaceObject> m_spaceObjects;
	std::unordered_map<SpaceID, size_t> m_IDToIndex;
	std::vector<SpaceID> m_IDs;
};
