#pragma once

#include "Game/Entity/SpaceID.h"

struct SpaceObject
{
	SpaceObject(ComponentManager* a_data, SpaceID a_ID)
		: m_ID(a_ID), m_entity(a_data) {}

	SpaceObject(EntityBase a_entity, SpaceID a_ID)
		: m_ID(a_ID), m_entity(a_entity) {}

	SpaceID m_ID;
	EntityBase m_entity;
};
