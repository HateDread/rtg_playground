#pragma once

#include <vector>
#include "Game/Observer/ObserverPhysics.h"
#include "Game/GameWorld.h"

struct ObserverPhysBuffers;
class GameWorld;

class ObserverManager
{
public:
	void CreateObservers(unsigned int a_numObservers, const GameWorld& a_realWorld);

	std::vector<ObserverPhysBuffers>	m_observerPhysics; 		
	std::vector<GameWorld>				m_observerWorlds;	
	std::vector<ObserverComponents>		m_observerComponents;
};
