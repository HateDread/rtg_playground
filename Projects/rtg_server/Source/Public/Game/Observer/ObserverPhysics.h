#pragma once

#include "Game/Physics/PhysicsState.h"
#include <vector>
#include "Game/Entity/Components.h"

struct CombinedPhys
{
	PhysicsState nextState;
	unsigned int nextStateIdx;
};

struct ObserverPhysBuffers
{
	std::vector<CombinedPhys>	m_phys;
	std::vector<PhysicsState>	m_currStates;
	glm::dvec3					m_viewLocation;
};

struct ObserverComponents
{
	std::vector<PosCom>*		m_posComs;
	std::vector<VelCom>*		m_velComs;
	std::vector<OrientCom>*		m_orientComs;
};
