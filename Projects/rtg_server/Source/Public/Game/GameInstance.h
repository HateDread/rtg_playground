#pragma once

#include <string>
#include <memory>
#include "Game\Physics\PhysicsStateQueue.h"
#include "Game\Observer\ObserverManager.h"

class GameWorld;
struct GameLevel;
class ObjectConstructor;

class GameInstance
{
public:
	GameInstance();
	~GameInstance();

public:
	void LoadGame(const ObjectConstructor& a_objectConstructor);
	PhysicsStateQueue& GetGlobalPhysicsStates();
	ObserverManager& GetObserverManager();
	const GameWorld* GetWorld() const;

private:
	PhysicsStateQueue	m_globalPhysicsStates;
	ObserverManager		m_observerManager;

	/*GameLevel LoadLevel(const std::string& a_levelName);*/
	std::unique_ptr<GameWorld> CreateWorld(const GameLevel& a_gameLevel);
	std::unique_ptr<GameWorld> m_realWorld;
};
