#pragma once

#include <memory>

class GameInstance;
class PhysicsManager;
class ObserverManager;
class PhysicsStateQueue;
struct ArrayedPhysicsData;

class GameFramework
{
public:
	GameFramework();
	~GameFramework();

	void LoadGame();
	void StartGame();
	void Update(double a_deltaTime);

	GameInstance* GetGameInstance();

	double GetWorldTime() const;

private:

	std::unique_ptr<GameInstance> m_gameInstance;
	std::unique_ptr<PhysicsManager> m_physics;

	double m_worldTime = 0;
	double m_lightSpeed2 = 8.98755179e+16;
	bool m_bShouldUpdate = false;
};
