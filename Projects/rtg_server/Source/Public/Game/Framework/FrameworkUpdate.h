#pragma once

#include "Game/Physics/ArrayedPhysicsData.h"

class PhysicsManager;
class GameInstance;
struct ArrayPhysicsData;
class ObserverManager;
class PhysicsStateQueue;

namespace FrameworkUpdate
{
	ArrayedPhysicsData UpdatePhysics(double a_deltaTime, PhysicsManager* a_physics, GameInstance* a_gameInstance);
	void UpdateObserverViewLocations(const ArrayedPhysicsData& a_physData, GameInstance* a_gameInstance);
	void BroadcastPhysics(double a_deltaTime, double a_worldTime, const ArrayedPhysicsData& a_physData, GameInstance* a_gameInstance);
	void UpdateObserverPhysics(double a_worldTime, double a_lightSpeed2, ObserverManager* a_observerManager, const PhysicsStateQueue& a_globalPhysBuffer);
};

