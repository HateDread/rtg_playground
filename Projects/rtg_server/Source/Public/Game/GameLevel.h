#pragma once

#include <glm/common.hpp>
#include <vector>

struct GameLevel
{
	std::vector<glm::dvec3> positions;
};
