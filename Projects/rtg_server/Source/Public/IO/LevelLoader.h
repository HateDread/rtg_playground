#pragma once

#include <string>
#include "Game\GameLevel.h"

class LevelLoader
{
public:
	explicit LevelLoader(const std::string& a_levelName);
	GameLevel operator()() const;

private:
	std::string m_levelName;

};
