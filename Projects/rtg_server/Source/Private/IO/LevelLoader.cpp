#include "IO/LevelLoader.h"
//#include <sol/sol.hpp>
#include <vector>

LevelLoader::LevelLoader(const std::string& a_levelName)
	: m_levelName(a_levelName)
{

}

GameLevel LevelLoader::operator()() const
{
	////// Sol code removed and library unlinked until Lua lib can be rebuilt with matching runtime

	// testing out some lua-driven 'level loading'

	/*sol::state lua;
	lua.script_file(m_levelName + ".lua");

	struct ShipStart
	{
		ShipStart(int a_x, int a_y, int a_z) : x(a_x), y(a_y), z(a_z) {}
		int x, y, z;
	};

	sol::table starts = lua["ShipStarts"];
	sol::table firstStart = starts[1];

	std::vector<ShipStart> startVec;
	startVec.emplace_back(firstStart[1], firstStart[2], firstStart[3]);*/

	// default construct for now until we can figure out
	// what this needs and what form it will take
	GameLevel level = GameLevel();
	level.positions.push_back(glm::dvec3(0, 0, 0));
	level.positions.push_back(glm::dvec3(0, 0, 5000));
	level.positions.push_back(glm::dvec3(0, 0, -5000));

	return level;
}
