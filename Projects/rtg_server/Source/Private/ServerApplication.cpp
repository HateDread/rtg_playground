#include "ServerApplication.h"

#include "Game/Framework/GameFramework.h"
#include "Network/Handlers/NetHandlers.h"
#include "Network/NetworkSystem.h"

#include <chrono>
#include <iostream>

ServerApplication::ServerApplication(const CommandArgs& a_commandArgs)
	: Application(a_commandArgs)
{

}

ServerApplication::~ServerApplication()
{

}

bool ServerApplication::Startup()
{
	// Parse commandline args / configs ( We're given these as key/value pairs, i.e. as CommandArgs, but these aren't stored at the moment! )
	// Early asset/safety & sanity checks (are required scripts present etc)

	// Create systems
	m_network = std::make_unique<Network::NetworkSystem>();
	m_game = std::make_unique<GameFramework>();
	m_handlers = std::make_unique<Network::NetHandlers>(m_game.get(), m_network->GetPublisher());

	// IF commandline args / configs specify a map, etc:
		// load it and get ready
		// IF started by client directly:
			// Tell them we're ready?

	// Wait for further commands/connections
	printf("Server awaiting commands...\n");

	m_game->LoadGame();

	if (m_commandArgs.ContainsPhrase("fastStart"))
	{
		printf("'fastStart' command detected. Launching game...\n\n");
		m_game->StartGame();
	}

	return true;
}

void ServerApplication::Shutdown()
{
	
}

void ServerApplication::Update(double a_deltaTime)
{
	m_frameTimer.accumulator += a_deltaTime;

	while (m_frameTimer.accumulator >= m_frameTimer.step)
	{
		double timeBefore = (double)std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch()).count();

		DoUpdate(m_frameTimer.step);

		double timeAfter = (double)std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch()).count();

		static double totalTime = 0;
		totalTime += timeAfter - timeBefore;
		//static int numSteps = 0;
		//numSteps++;

		//printf("Avg StepTime: %f\n", totalTime / numSteps);

		m_frameTimer.accumulator -= m_frameTimer.step;
	}
}

void ServerApplication::DoUpdate(double a_deltaTime)
{
	// Update network to get packets, fire off callbacks, etc
	m_network->Update();

	// Other systems update here
	m_game->Update(a_deltaTime);

	// Collect queued NetworkMessage objects (or similar) and 
	// send out to clients
	m_network->PostUpdate();
}
