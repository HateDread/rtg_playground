#include "Network/SendMessage.h"
#include "Network/NetClient.h"

#include <RakPeerInterface.h>
#include <BitStream.h>

#include <mutex>
#include <assert.h>
#include <vector>

namespace Network
{
namespace Outgoing
{
namespace
{

struct TransData
{
	std::vector<ClientID>	m_targetIDs;
	PacketPriority			m_priority;
	PacketReliability		m_reliability;
	bool					m_broadcastAll;
	int						m_orderingChannel = 0;
};

class PreppedMessage
{
public:
	PreppedMessage(const RakNet::BitStream& a_message, const TransData& a_transData)
		: m_stream(a_message.GetData(), a_message.GetNumberOfBytesUsed(), true), 
		m_transData(std::move(a_transData)) {}

	PreppedMessage(PreppedMessage&& a_rhs)
		: m_stream(a_rhs.m_stream.GetData(), a_rhs.m_stream.GetNumberOfBytesUsed(), true),
		m_transData(std::move(a_rhs.m_transData)) {}

	RakNet::BitStream	m_stream;
	TransData			m_transData;
};

std::vector<PreppedMessage> messages;
std::mutex messageGuard;

}

void EnqueueStream(const RakNet::BitStream& a_message, 
	const std::vector<ClientID>& a_targetIDs, PacketPriority a_priority, 
	PacketReliability a_reliability, int a_orderingChannel /*= 0*/)
{
	std::lock_guard<std::mutex> guard(messageGuard);

	messages.emplace_back(PreppedMessage(a_message, { std::move(a_targetIDs), a_priority, a_reliability, false, a_orderingChannel }));
}

void BroadcastStream(const RakNet::BitStream& a_message, PacketPriority a_priority, 
	PacketReliability a_reliability, int a_orderingChannel /*= 0*/)
{
	std::lock_guard<std::mutex> guard(messageGuard);

	messages.emplace_back(PreppedMessage(a_message, { { /* No IDs */ }, a_priority, a_reliability, true, a_orderingChannel }));
}

void SendAll(RakNet::RakPeerInterface* a_raknet)
{
	std::lock_guard<std::mutex> guard(messageGuard);

	for (const auto& message : messages)
	{
		// do we ever want to send to more than 1 client but not all? Team messages in multiplayer?
		const std::vector<Network::ClientID>& targetIDs = message.m_transData.m_targetIDs;

		std::vector<RakNet::RakNetGUID> targetGUIDs;
		targetGUIDs.reserve(targetIDs.size());

		// send selectively
		if (!message.m_transData.m_broadcastAll)
		{
			// grab valid network GUIDs for target Clients
			for (ClientID id : targetIDs)
			{
				const auto guid = Network::GetConnectionGUID(id);
				if (guid != RakNet::UNASSIGNED_RAKNET_GUID)
				{
					targetGUIDs.push_back(guid);
				}
			}

			// send message as a packet to the correct GUIDs
			for (const auto& guid : targetGUIDs)
			{
				a_raknet->Send(&message.m_stream, message.m_transData.m_priority, message.m_transData.m_reliability, 
					message.m_transData.m_orderingChannel, guid, false);
			}
		}
		else // send to all
		{
			a_raknet->Send(&message.m_stream, message.m_transData.m_priority, message.m_transData.m_reliability, 
				message.m_transData.m_orderingChannel, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
		}
	}

	messages.clear();
}

} // namespace Outgoing
} // namespace Network
