#include "Network/ClientSync.h"
#include "Network/PacketTypes.h"
#include "Network/SendMessage.h"
#include "Network/Packets/SessionPackets.h"

#include <BitStream.h>
#include <PacketPriority.h>

#include <memory>

namespace Network
{

void SyncManager::BeginSync(const ServerInitialSyncMsg& a_syncMsg, const SyncCallback& a_syncCallback /*= SyncCallback()*/)
{
	assert(m_loadingPlayers.find(a_syncMsg.clientID) == m_loadingPlayers.end());
	m_loadingPlayers.emplace(a_syncMsg.clientID, a_syncCallback);

	Outgoing::EnqueueMessage(a_syncMsg, { a_syncMsg.clientID }, HIGH_PRIORITY, RELIABLE_ORDERED);
}

void SyncManager::OnSyncAck(ClientID a_clientID)
{
	auto it = m_loadingPlayers.find(a_clientID);
	assert(it != m_loadingPlayers.end());

	auto func = std::move(it->second);
	m_loadingPlayers.erase(it);

	m_numLoaded++;

	if (func)
	{
		func(true);
	}
}

int SyncManager::NumPlayersLoading()
{
	return (int)m_loadingPlayers.size();
}

int SyncManager::NumPlayersLoaded()
{
	return m_numLoaded;
}

void SyncManager::ResetNumLoaded()
{
	m_numLoaded = 0;
}

} // namespace Network
