#include "Network/NetClient.h"

#include <RakNetTypes.h>

#include <unordered_map>
#include <algorithm>
#include <assert.h>

namespace Network
{
namespace
{

ClientID nextID = 0;
std::unordered_map<ClientID, RakNet::RakNetGUID> idToGUID;

const RakNet::RakNetGUID INVALID_GUID = RakNet::UNASSIGNED_RAKNET_GUID;

}

ClientID RegisterClient(const RakNet::RakNetGUID& a_netGUID)
{
	// unique GUIDs
	assert(std::find_if(idToGUID.begin(), idToGUID.end(), [a_netGUID](const auto& a_pair)
	{
		return a_pair.second == a_netGUID;
	}) == idToGUID.end());

	ClientID newID = nextID++;
	idToGUID.insert(std::pair<ClientID, RakNet::RakNetGUID>(newID, a_netGUID));
	return newID;
}

void RemoveClient(ClientID a_clientID)
{
	auto it = idToGUID.find(a_clientID);
	assert(it != idToGUID.end());

	idToGUID.erase(it);
}

void RemoveClient(const RakNet::RakNetGUID& a_netGUID)
{
	auto it = std::find_if(idToGUID.begin(), idToGUID.end(), [&a_netGUID](const auto& pair) {
		return a_netGUID == pair.second;
	});
	assert(it != idToGUID.end());

	idToGUID.erase(it);
}

const RakNet::RakNetGUID& Network::GetConnectionGUID(ClientID a_clientID)
{
	auto it = idToGUID.find(a_clientID);
	if (it != idToGUID.end())
	{
		return it->second;
	}
	else
	{
		return INVALID_GUID;
	}
}

Network::ClientID GetClientID(const RakNet::RakNetGUID& a_netGUID)
{
	for (const auto& pair : idToGUID)
	{
		if (pair.second == a_netGUID)
		{
			return pair.first;
		}
	}
	return INVALID_CLIENT;
}

int NumClients()
{
	return (int)idToGUID.size();
}

} // namespace Network
