#include "Network/NetworkSystem.h"
#include "Network/NetworkPublisher.h"
#include "Network/SendMessage.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>

#include <vector>

namespace Network
{

NetworkSystem::NetworkSystem()
{
	m_raknet = RakNet::RakPeerInterface::GetInstance();

	RakNet::SocketDescriptor desc(12001, 0);
	m_raknet->Startup(16, &desc, 1);
	m_raknet->SetMaximumIncomingConnections(16);

	m_publisher = std::make_unique<NetworkPublisher>();
}

NetworkSystem::~NetworkSystem()
{
	m_raknet->Shutdown(100);
	RakNet::RakPeerInterface::DestroyInstance(m_raknet);
}

void NetworkSystem::Update()
{
	std::vector<RakNet::Packet*> packets;

	RakNet::Packet* packet = m_raknet->Receive();
	while (packet != nullptr)
	{
		packets.push_back(packet);
		packet = m_raknet->Receive();
	}

	// pass through packets?
	GetPublisher()->Update(packets);

	// clean up packets and resize vector without shrinking memory
	for (int i = 0; i < packets.size(); ++i)
	{
		m_raknet->DeallocatePacket(packets[i]);
	}
	packets.resize(0);

	// loop through once done and call m_raknet->DeallocatePacket(packet) ??
	//// or we have to process immediately and deallocate each time for Receive() to work properly?
}

void NetworkSystem::PostUpdate()
{
	Outgoing::SendAll(m_raknet);
}

NetworkPublisher* NetworkSystem::GetPublisher()
{
	return m_publisher.get();
}

} // namespace Network
