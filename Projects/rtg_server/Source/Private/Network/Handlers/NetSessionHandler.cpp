#include "Network/Handlers/NetSessionHandler.h"
#include "Game/Framework/GameFramework.h"
#include "Network/NetworkPublisher.h"
#include "Network/ClientSync.h"
#include "Network/PacketTypes.h"
#include "Network/SendMessage.h"
#include "Network/Packets/SessionPackets.h"

#include <MessageIdentifiers.h>
#include <RakNetTypes.h>


namespace
{
Network::SyncManager syncManager;
}

namespace Network
{

NetSessionHandler::NetSessionHandler(GameFramework* a_gameFramework, NetworkPublisher* a_networkPublisher)
	: m_gameFramework(a_gameFramework)
{
	a_networkPublisher->Register(ID_NEW_INCOMING_CONNECTION, PacketCallback(&NetSessionHandler::OnNewConnection, this));
	a_networkPublisher->Register(ID_CONNECTION_LOST, PacketCallback(&NetSessionHandler::OnDisconnection, this));
	a_networkPublisher->Register(ID_DISCONNECTION_NOTIFICATION, PacketCallback(&NetSessionHandler::OnDisconnection, this));

	a_networkPublisher->Register(EPacketType::CLIENT_SYNC_ACK, PacketCallback(&NetSessionHandler::OnClientSyncAck, this));
}

void NetSessionHandler::OnNewConnection(const RakNet::Packet* a_packet)
{
	ClientID id = Network::RegisterClient(a_packet->guid);

	Network::ServerInitialSyncMsg syncMsg{ id, 666 };
	syncManager.BeginSync(syncMsg, [id, this](bool a_success) {
		printf("Player %u acked sync!\n", id);

		// replace hard-coding with some global config?
		if (Network::NumClients() == 1 && syncManager.NumPlayersLoaded() == Network::NumClients())
		{
			m_gameFramework->StartGame();
		}
	});
}

void NetSessionHandler::OnDisconnection(const RakNet::Packet* a_packet)
{
	ClientID clientID = Network::GetClientID(a_packet->guid);
	Network::RemoveClient(clientID);

	printf("Client %d disconnected!\n", clientID);

	// send out notification to all other players
	Outgoing::BroadcastMessage(ServerClientDisconnectedMsg{ clientID }, HIGH_PRIORITY, RELIABLE);
}

void NetSessionHandler::OnClientSyncAck(const RakNet::Packet* a_packet)
{
	auto id = Network::GetClientID(a_packet->guid);
	assert(id != Network::INVALID_CLIENT);

	syncManager.OnSyncAck(id);
}

} // namespace Network
