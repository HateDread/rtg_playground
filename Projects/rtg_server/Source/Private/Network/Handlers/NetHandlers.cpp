#include "Network/Handlers/NetHandlers.h"
#include "Network/Handlers/NetSessionHandler.h"
#include "CommandArgs.h"

namespace Network
{

NetHandlers::NetHandlers(GameFramework* a_gameFramework, NetworkPublisher* a_networkPublisher)
	: m_sessionHandler(std::make_unique<NetSessionHandler>(a_gameFramework, a_networkPublisher))
{

}

NetHandlers::~NetHandlers() = default;

} // namespace Network
