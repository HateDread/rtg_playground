#include "Game/Physics/PhysicsStateQueue.h"

void PhysicsStateQueue::CreateNewStates(const ArrayedPhysicsData& a_physData, double a_timestamp)
{
	for (unsigned int i = 0; i < a_physData.positions.size(); ++i)
	{
		m_objectPhysStates[i].emplace_back(a_physData.positions[i], a_physData.velocities[i], 
											a_physData.orientations[i], (float)a_timestamp);

	}
}

void PhysicsStateQueue::ReserveSpaceForObjects(int a_numObjects)
{
	unsigned int oldSize = (unsigned int)m_objectPhysStates.size();
	m_objectPhysStates.resize(m_objectPhysStates.size() + a_numObjects);

	for (unsigned int i = oldSize; i < m_objectPhysStates.size(); ++i)
	{
		m_objectPhysStates[i].reserve(50000);
	}
}
