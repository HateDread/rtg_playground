#include "Game/Physics/PhysicsBody.h"
#include "Jaylen/JayObject.h"

#include <assert.h>

PhysicsBody::PhysicsBody(JayObject* a_jayObject)
	: m_jayObject(a_jayObject)
{
	assert(m_jayObject != nullptr);
}

glm::dvec3 PhysicsBody::GetPosition() const
{
	return m_jayObject->GetPosition();
}

glm::vec3 PhysicsBody::GetVelocity() const
{
	return m_jayObject->GetVelocity();
}

glm::vec3 PhysicsBody::GetForce() const
{
	return m_jayObject->GetForce();
}

float PhysicsBody::GetMass() const
{
	return m_jayObject->GetMass();
}

glm::quat PhysicsBody::GetOrientation() const
{
	return m_jayObject->GetOrientation();
}

glm::vec3 PhysicsBody::GetAngularVelocity() const
{
	return m_jayObject->GetAngularVelocity();
}

glm::vec3 PhysicsBody::GetTorque() const
{
	return m_jayObject->GetTorque();
}

float PhysicsBody::GetMomentOfInertia() const
{
	return m_jayObject->GetMomentOfInertia();
}

void PhysicsBody::AddForce(const glm::vec3& a_force)
{
	m_jayObject->AddForce(a_force);
}

void PhysicsBody::AddTorque(const glm::vec3& a_torque)
{
	m_jayObject->AddTorque(a_torque);
}

void PhysicsBody::AddRelativeForce(const glm::vec3& a_force)
{
	m_jayObject->AddRelativeForce(a_force);
}

void PhysicsBody::AddRelativeTorque(const glm::vec3& a_torque)
{
	m_jayObject->AddRelativeTorque(a_torque);
}

void PhysicsBody::SetVelocity(const glm::vec3& a_velocity)
{
	m_jayObject->SetVelocity(a_velocity);
}

void PhysicsBody::SetRelativeVelocity(const glm::vec3& a_velocity)
{
	m_jayObject->SetRelativeVelocity(a_velocity);
}

void PhysicsBody::SetPosition(const glm::dvec3& a_position)
{
	m_jayObject->SetPosition(a_position);
}

void PhysicsBody::SetOrientation(const glm::quat& a_orientation)
{
	m_jayObject->SetOrientation(a_orientation);
}

void PhysicsBody::SetOrientation(const glm::vec3& a_euler)
{
	m_jayObject->SetOrientation(a_euler);
}

void PhysicsBody::SetMass(float a_mass)
{
	m_jayObject->SetMass(a_mass);
}

void PhysicsBody::SetMomentOfInertia(float a_momentOfInertia)
{
	m_jayObject->SetMomentOfInertia(a_momentOfInertia);
}

void PhysicsBody::SetOrientationRad(const glm::vec3& a_rads)
{
	m_jayObject->SetOrientationRad(a_rads);
}

void PhysicsBody::SetOrientation(float a_angle, const glm::vec3& a_axis)
{
	m_jayObject->SetOrientation(a_angle, a_axis);
}
