#include "Game\Physics\PhysicsManager.h"
#include "Jaylen\Jaylen.h"
#include "Jaylen\JayScene.h"

#include <assert.h>
#include "Game\Physics\PhysicsBody.h"
#include "Game\Physics\ArrayedPhysicsData.h"

PhysicsManager::PhysicsManager()
	: m_jaylen(std::make_unique<Jaylen>())
{

}

PhysicsManager::~PhysicsManager() = default;

void PhysicsManager::CreateScene(unsigned int a_maxObjects)
{
	m_jaylen->CreateScene(a_maxObjects);
}

void PhysicsManager::Update(double a_deltaTime)
{
	auto scene = m_jaylen->GetScene();

	assert(scene != nullptr);

	scene->Step(a_deltaTime);
}

void PhysicsManager::FillArrayedData(ArrayedPhysicsData& a_arrayedPhysData)
{
	auto& data = m_jaylen->GetScene()->GetData();

	// we only reserve enough for m_physicsBodies.size()
	size_t numObjects = m_physicsBodies.size();

	a_arrayedPhysData.positions.resize(numObjects);
	a_arrayedPhysData.velocities.resize(numObjects);
	a_arrayedPhysData.orientations.resize(numObjects);

	memcpy(a_arrayedPhysData.positions.data(), data.m_positions.data(), sizeof(glm::dvec3) * numObjects);
	memcpy(a_arrayedPhysData.velocities.data(), data.m_velocities.data(), sizeof(glm::vec3) * numObjects);
	memcpy(a_arrayedPhysData.orientations.data(), data.m_orientations.data(), sizeof(glm::quat) * numObjects);
}

PhysicsBody PhysicsManager::CreatePhysicsBody()
{
	assert(m_jaylen->GetScene() != nullptr);

	JayObject* obj = m_jaylen->GetScene()->CreateObject();
	return *m_physicsBodies.emplace(m_physicsBodies.end(), obj);
}

void PhysicsManager::DestroyPhysicsBody(const PhysicsBody& a_physicsBody)
{
	auto it = std::find(m_physicsBodies.begin(), m_physicsBodies.end(), a_physicsBody);
	if (it != m_physicsBodies.end())
	{
		// tell Jaylen to destroy the (*it).m_jayObject
		m_physicsBodies.erase(it);
	}
}
