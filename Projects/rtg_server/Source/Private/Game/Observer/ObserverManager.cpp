#include "Game/Observer/ObserverManager.h"
#include "Game/Entity/Components.h"

void ObserverManager::CreateObservers(unsigned int a_numObservers, const GameWorld& a_realWorld)
{
	size_t oldSize = m_observerPhysics.size();
	size_t newSize = oldSize + a_numObservers;

	m_observerPhysics.resize(newSize);
	m_observerWorlds.resize(newSize, a_realWorld);

	// tell the new observers about all pre-existing objects
	auto& realObjs = a_realWorld.GetEntities();
	size_t numObjs = realObjs.get<PosCom>()->get().size();

	// prepare to cache the important components for each observer
	m_observerComponents.resize(newSize);

	for (size_t i = oldSize; i < newSize; ++i)
	{
		// provide a place to put the CombinedPhys struct for each obj for observer[i]
		m_observerPhysics[i].m_phys.resize(numObjs);

		// cache observer component vectors for later use
		ComponentManager& observerComps = m_observerWorlds[i].GetEntities();

		m_observerComponents[i].m_posComs = &observerComps.get<PosCom>()->get();
		m_observerComponents[i].m_velComs = &observerComps.get<VelCom>()->get();
		m_observerComponents[i].m_orientComs = &observerComps.get<OrientCom>()->get();
	}
}
