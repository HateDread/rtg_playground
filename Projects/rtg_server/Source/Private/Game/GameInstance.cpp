#include "Game/GameInstance.h"
#include "Game/GameWorld.h"
#include "Game/GameLevel.h"
#include "IO/LevelLoader.h"

#include "Game/Entity/ObjectConstructor.h"
#include "Game/Entity/Components.h"
#include "Game/Entity/ECS.h"

GameInstance::GameInstance() = default;
GameInstance::~GameInstance() = default;

void GameInstance::LoadGame(const ObjectConstructor& a_objectConstructor)
{
	// Eventually feed in some command/args-driven map name?
	/*GameLevel level = LoadLevel("TestLevel");
	m_world = CreateWorld(level);*/

	// Start the game...

	m_realWorld = CreateWorld(GameLevel());

	// test code - make some ships and give some of them a non-zero velocity
	auto& comps = m_realWorld->GetEntities();
	for (unsigned int i = 0; i < 100; ++i)
	{
		auto ship = a_objectConstructor.CreateShip(*m_realWorld.get(), m_observerManager, m_realWorld->GetSpaceObjects());
		auto physCom = comps.get<PhysCom>(ship.m_entity);

		physCom->body.SetPosition(glm::dvec3(1000000.0*i + 1000000.0,(i % 2) * 1000000.0, -1000000.0));
		physCom->body.SetMass(30.0f);
		physCom->body.SetMomentOfInertia(30.0f);

		// move some objects, but not all
		if (i % 2)
		{
			physCom->body.AddRelativeForce(glm::vec3(10000.0f, 0.0f, 0.0f));
		}
	}

	// test code - set up physics states
	auto& objs = m_realWorld->GetEntities();
	unsigned int numObs = (unsigned int)objs.get<PosCom>()->size();
	GetGlobalPhysicsStates().ReserveSpaceForObjects(numObs);

	// setup observers to clone real objects
	m_observerManager.CreateObservers(numObs, *m_realWorld.get());

	std::vector<PosCom>* positions = &objs.get<PosCom>()->get();
	std::vector<VelCom>* velocities = &objs.get<VelCom>()->get();
	std::vector<OrientCom>* orientations = &objs.get<OrientCom>()->get();

	// test code - set up currStates
	for (unsigned int i = 0; i < numObs; ++i)
	{
		m_observerManager.m_observerPhysics[i].m_currStates.reserve(positions->size());
		for (unsigned int j = 0; j < positions->size(); ++j)
		{
			// create at least one current PhysicsState for each ship for each observer
			m_observerManager.m_observerPhysics[i].m_currStates.emplace_back((*positions)[j].p, (*velocities)[j].v, (*orientations)[j].o, 0.0f);
		}
	}
}

PhysicsStateQueue& GameInstance::GetGlobalPhysicsStates()
{
	return m_globalPhysicsStates;
}

ObserverManager& GameInstance::GetObserverManager()
{
	return m_observerManager;
}

const GameWorld* GameInstance::GetWorld() const
{
	return m_realWorld.get();
}

//GameLevel GameInstance::LoadLevel(const std::string& a_levelName)
//{
//	LevelLoader loader(a_levelName);
//	return loader();
//}

std::unique_ptr<GameWorld> GameInstance::CreateWorld(const GameLevel& a_gameLevel)
{
	// replace with some GameWorld factory or similar?
	std::unique_ptr<GameWorld> world = std::make_unique<GameWorld>();
	// world->InitializeLevel(a_gameLevel)??
	return world;
}

