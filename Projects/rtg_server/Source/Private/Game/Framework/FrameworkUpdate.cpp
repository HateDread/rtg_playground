#include "Game/Framework/FrameworkUpdate.h"
#include "Game/Observer/ObserverManager.h"
#include "Game/Physics/PhysicsStateQueue.h"
#include "Game/GameInstance.h"
#include "Game/Physics/PhysicsManager.h"
#include "glm/gtx/norm.hpp"

namespace FrameworkUpdate
{
	ArrayedPhysicsData UpdatePhysics(double a_deltaTime, PhysicsManager* a_physics, GameInstance* a_gameInstance)
	{
		a_physics->Update(a_deltaTime);

		// update our phys data
		ArrayedPhysicsData physData;
		a_physics->FillArrayedData(physData);

		auto comps = a_gameInstance->GetWorld()->GetEntities();
		auto positions = comps.get<PosCom>();
		auto velocities = comps.get<VelCom>();
		auto orientations = comps.get<OrientCom>();

		// update ECS components with new physics data
		memcpy(positions->compData(), physData.positions.data(), sizeof(glm::dvec3) * physData.positions.size());
		memcpy(velocities->compData(), physData.velocities.data(), sizeof(glm::vec3) * physData.velocities.size());
		memcpy(orientations->compData(), physData.orientations.data(), sizeof(glm::quat) * physData.orientations.size());

		return physData;
	}

	void UpdateObserverViewLocations(const ArrayedPhysicsData& a_physData, GameInstance* a_gameInstance)
	{
		// for now we just map 1:1
		auto& obs = a_gameInstance->GetObserverManager();
		for (unsigned int i = 0; i < obs.m_observerPhysics.size(); ++i)
		{
			obs.m_observerPhysics[i].m_viewLocation = a_physData.positions[i];
		}
	}

	void BroadcastPhysics(double a_deltaTime, double a_worldTime, const ArrayedPhysicsData& a_physData, GameInstance* a_gameInstance)
	{
		auto& states = a_gameInstance->GetGlobalPhysicsStates();

		states.CreateNewStates(a_physData, a_worldTime);

		// Need timestamps and observers, so we can iterate states and observers and figure out what's arrived.
		//// Also need latest indices for each object in the phys queue so we know where to start checking.
	}

	void UpdateObserverPhysics(double a_worldTime, double a_lightSpeed2, ObserverManager* a_observerManager, const PhysicsStateQueue& a_globalPhysBuffer)
	{
		std::vector<ObserverPhysBuffers>& obsBuffers = a_observerManager->m_observerPhysics;

		// iterate all Observers
		for (unsigned int i = 0; i < obsBuffers.size(); ++i)
		{
			std::vector<CombinedPhys>& phys = obsBuffers[i].m_phys;
			glm::dvec3 viewLocation = obsBuffers[i].m_viewLocation;

			// reserve our 'new'/replacement state buffer up-front (one state for each object known to Observer)
			std::vector<PhysicsState> newStates(phys.size());

			// this assumes 100% state miss rate, which is unlikely, 
			// but could outweigh constant realloc from pushing
			std::vector<std::pair<unsigned int, PhysicsState>> statesToExtrapolate;
			statesToExtrapolate.reserve(phys.size());

			// iterate over a CombinedPhys struct for each object known to Observer
			for (unsigned int j = 0; j < phys.size(); ++j)
			{
				// quick and dirty way to not observe self
				if (i == j)
					continue;

				const PhysicsState* latestState = &phys[j].nextState;
				unsigned int stateIdx = phys[j].nextStateIdx;

				double dist = glm::distance2(viewLocation, latestState->pos);
				float timeSec = (float)(dist / a_lightSpeed2); // use squared

															   //// next state arrived 
				if (timeSec < a_worldTime - latestState->time)
				{
					const auto& stateBuffer = a_globalPhysBuffer[j];

					// start checking if it's the only one to arrive this frame (Doppler effect means multiple per frame if high velocity)
					while (true && (stateIdx + 1) < stateBuffer.size())
					{
						// the chances of 100% catching up to real obj state are tiny. Maybe assert in Debug?
						double dist = glm::distance2(viewLocation, stateBuffer[++stateIdx].pos);
						float timeSec = (float)(dist / a_lightSpeed2); // use squared

																	   // there's a newer arrived state than what we thought previously
						if (timeSec < a_worldTime - stateBuffer[stateIdx].time)
						{
							latestState = &stateBuffer[stateIdx];
						}
						else
						{
							stateIdx--;
							break;
						}
					}

					// we've figured out which state it is
					newStates[j] = (*latestState);

					// we haven't caught up to real time yet
					if (stateIdx + 1 < stateBuffer.size())
					{
						// replace the combination struct with the next state and its index
						phys[j].nextState = stateBuffer[++stateIdx];
						phys[j].nextStateIdx = stateIdx;
						// replace current state for this object in this observer
						obsBuffers[i].m_currStates[j] = *latestState;
					}
				}
				else //// no states arrived for this obj - mark for extrapolation
				{
					statesToExtrapolate.emplace_back(j, obsBuffers[i].m_currStates[j]);
				}
			}

			// split extrapolation out into separate loop to keep above branches small
			for (unsigned int j = 0; j < statesToExtrapolate.size(); ++j)
			{
				PhysicsState& currentState = statesToExtrapolate[j].second;

				// perform extrapolation
				PhysicsState extrapolatedState;
				glm::dvec3 newPos = currentState.pos + (glm::dvec3(currentState.vel) * (currentState.time - a_worldTime));
				extrapolatedState.pos = newPos;
				extrapolatedState.vel = currentState.vel;
				extrapolatedState.orient = currentState.orient;
				extrapolatedState.time = (float)a_worldTime;

				newStates[statesToExtrapolate[j].first] = extrapolatedState;
			}

			// cached ptrs to ECS component vectors (otherwise hash-map look-up)
			auto& cachedComponents = a_observerManager->m_observerComponents;

			std::vector<PosCom>& positions = *cachedComponents[i].m_posComs;
			std::vector<VelCom>& velocities = *cachedComponents[i].m_velComs;
			std::vector<OrientCom>& orientations = *cachedComponents[i].m_orientComs;

			// apply buffered state updates
			for (unsigned int j = 0; j < newStates.size(); ++j)
			{
				positions[j].p = newStates[j].pos;
				velocities[j].v = newStates[j].vel;
				orientations[j].o = newStates[j].orient;
			}
		}
	}
}