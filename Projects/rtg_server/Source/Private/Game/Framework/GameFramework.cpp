#include "Game/Entity/Components.h"
#include "Game/Entity/ObjectConstructor.h"
#include "Game/Framework/FrameworkUpdate.h"
#include "Game/Framework/GameFramework.h"
#include "Game/GameInstance.h"
#include "Game/Observer/ObserverPhysics.h"
#include "Game/Physics/PhysicsManager.h"

#include <glm/gtx/norm.hpp>

GameFramework::GameFramework() = default;
GameFramework::~GameFramework() = default;

void GameFramework::LoadGame()
{
	printf("GameFramework -> Loading Game\n");

	m_worldTime = 0;

	m_physics = std::make_unique<PhysicsManager>();
	m_physics->CreateScene(10000);

	ObjectConstructor objConstructor(m_physics.get());

	m_gameInstance = std::make_unique<GameInstance>();
	m_gameInstance->LoadGame(objConstructor);

	// move the world forward one step so we have velocity applied at start
	//ArrayedPhysicsData physData = FrameworkUpdate::UpdatePhysics(1 / 60.0, m_physics.get(), m_gameInstance.get());
	m_worldTime += 1 / 60.0;

	// fill the 'nextState' of each observer's view of each object with actual data
	// gross for now, but here to make sure that the check for first PhysicsState arrival is with real pos/timestamp
	//auto& obsManager = m_gameInstance->GetObserverManager();
	//auto& obs = obsManager.m_observerPhysics;
	//for (int i = 0; i < obs.size(); ++i)
	//{
	//	for (int j = 0; j < obs[i].m_phys.size(); ++j)
	//	{
	//		obs[i].m_phys[j].nextState = PhysicsState{ physData.positions[j], physData.velocities[j],
	//			physData.orientations[j], (float)m_worldTime };
	//	}
	//}
}

void GameFramework::StartGame()
{
	printf("GameFramework -> Starting Game\n");// with %d connected players!\n", m_playerList->GetNumPlayers());

	// allow updates now we've started
	m_bShouldUpdate = true;
}

void GameFramework::Update(double a_deltaTime)
{
	// maybe we haven't started the game yet?
	if (!m_bShouldUpdate)
		return;

	m_worldTime += a_deltaTime;

	// Update Physics
	ArrayedPhysicsData newPhysData(FrameworkUpdate::UpdatePhysics(a_deltaTime, m_physics.get(), m_gameInstance.get()));
		// - Handle callbacks

	// Make sure observer view locations are updated ahead of observer physics
	FrameworkUpdate::UpdateObserverViewLocations(newPhysData, m_gameInstance.get());

	// Broadcast Physics
	FrameworkUpdate::BroadcastPhysics(a_deltaTime, m_worldTime, newPhysData, m_gameInstance.get());

	// Update observer physics
	FrameworkUpdate::UpdateObserverPhysics(m_worldTime, m_lightSpeed2, &m_gameInstance->GetObserverManager(), m_gameInstance->GetGlobalPhysicsStates());
		// - Consider merging the two - update in-place if observer pos' are contiguous
	// Update AI
	// Process real events
	// Process observer events

	// Scrape for info to send out to clients and enqueue it
}

GameInstance* GameFramework::GetGameInstance()
{
	return m_gameInstance.get();
}

double GameFramework::GetWorldTime() const
{
	return m_worldTime;
}

