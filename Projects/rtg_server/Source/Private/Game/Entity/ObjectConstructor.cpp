#include "Game/Entity/ObjectConstructor.h"
#include "Game/Entity/Components.h"
#include "Game/Physics/PhysicsManager.h"
#include "Game/Observer/ObserverManager.h"
#include "Game/GameWorld.h"
#include "Game/Entity/SpaceObjects.h"

SpaceID ObjectConstructor::m_nextID = 0;

ObjectConstructor::ObjectConstructor(PhysicsManager* a_physicsManager)
	: m_physicsManager(a_physicsManager)
{

}

SpaceObject ObjectConstructor::CreateSpaceObject(ComponentManager& a_components, ObserverManager& a_observerManager, SpaceObjects& a_spaceObjects) const
{
	EntityBase ent = CreateEntity(a_components);
	SpaceObject obj(ent, m_nextID++);
	a_spaceObjects.AddObject(obj);

	/*EntityBase obj{ &a_components };*/
	obj.m_entity.add<PosCom>(glm::dvec3(0.0f, 0.0f, 0.0f));

	obj.m_entity.add<VelCom>();
	obj.m_entity.add<OrientCom>(glm::quat());

	PhysicsBody body = m_physicsManager->CreatePhysicsBody();
	obj.m_entity.add<PhysCom>(body);

	// inform other observers about this space object
	//a_observerManager.RegisterNewObject(obj, a_components);

	return obj;
}

SpaceObject ObjectConstructor::CreateShip(GameWorld& a_realWorld, ObserverManager& a_observerManager, SpaceObjects& a_spaceObjects) const
{
	// this should handle informing other observers of the new object
	SpaceObject ship = CreateSpaceObject(a_realWorld.GetEntities(), a_observerManager, a_spaceObjects);
	
	// create observer
	//a_observerManager.CreateObservers(1, a_realWorld);

	// associate observer with ship

	return ship;
}

EntityBase ObjectConstructor::CreateEntity(ComponentManager& a_components) const
{
	EntityBase ent{ &a_components };
	return ent;
}
